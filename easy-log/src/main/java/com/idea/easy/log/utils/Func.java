package com.idea.easy.log.utils;

import com.idea.easy.log.utils.str.StringPool;
import org.springframework.util.StringUtils;

import java.lang.reflect.Array;
import java.util.*;
import java.util.function.Predicate;

/**
 * @className: Func
 * @description:
 * @author: salad
 * @date: 2022/5/28
 **/
public class Func {

    @SuppressWarnings("all")
    public static boolean isEmpty(Object obj) {
        if (obj == null) {
            return true;
        } else if (obj instanceof Optional) {
            return !((Optional)obj).isPresent();
        } else if (obj instanceof CharSequence) {
            return ((CharSequence)obj).length() == 0;
        } else if (obj.getClass().isArray()) {
            return Array.getLength(obj) == 0;
        } else if (obj instanceof Collection) {
            return ((Collection)obj).isEmpty();
        } else if (obj instanceof Number){
            return StringPool.ZERO.equals(obj.toString());
        }else{
            return obj instanceof Map && ((Map) obj).isEmpty();
        }
    }

    public static boolean isNotEmpty(Object obj){
        return !isEmpty(obj);
    }

    public static List<String> toList(String values,String separator){
        if (isBlank(values))
            return Collections.emptyList();
        return Arrays.asList(values.split(separator));
    }

    public static List<String> toList(String values){
        return toList(values, StringPool.COMMA);
    }

    public static String[] toArray(String values,String separator){
        if (isBlank(values))
            return new String[0];
        return values.split(separator);
    }

    public static String join(Collection<?> coll, String delimiter,
                              String prefix, String suffix,
                              String emptyValue, boolean isSkipEmpty){
        if (Func.isEmpty(coll)){
            return emptyValue;
        }

        Iterator<?> iterator = coll.iterator();
        StringJoiner joiner = new StringJoiner(delimiter,prefix,suffix);

        if (null != emptyValue)
            joiner.setEmptyValue(emptyValue);

        while (iterator.hasNext()){
            Object next = iterator.next();
            if (isSkipEmpty){
                if (null != next && isNotBlank(String.valueOf(next))){
                    joiner.add(String.valueOf(next));
                }
            }else {
                joiner.add(String.valueOf(next));
            }
        }
        return joiner.toString();
    }

    public static String join(Collection<?> coll,String delimiter,boolean isSkipEmpty){
        return join(coll,delimiter,StringPool.EMPTY,StringPool.EMPTY,StringPool.EMPTY,isSkipEmpty);
    }

    public static String join(Collection<?> coll,String delimiter){
        return join(coll,delimiter,false);
    }

    public static String join(Collection<?> coll){
        return join(coll,StringPool.COMMA);
    }

    public static <T> String join(T[] array){
        return join(Collections.singletonList(array),",");
    }

    public static boolean isBlank(final CharSequence cs) {
        return !StringUtils.hasText(cs);
    }

    public static boolean isNotBlank(final CharSequence cs) {
        return StringUtils.hasText(cs);
    }

    public static boolean eq(final CharSequence v1,final CharSequence v2) {
        return  eq(v1,v2,false);
    }

    public static boolean eqIgnoreCase(final CharSequence v1,final CharSequence v2) {
        return  eq(v1,v2,true);
    }

    public static boolean eq(CharSequence str1, CharSequence str2, boolean ignoreCase) {
        if (null == str1) {
            return str2 == null;
        } else if (null == str2) {
            return false;
        } else {
            return ignoreCase ? str1.toString().equalsIgnoreCase(str2.toString()) : str1.equals(str2);
        }
    }

    public static <T> T getValue(T src,T alternativeVal){
        return isEmpty(src) ? alternativeVal : src;
    }

    public static <T> T getValue(T src, Predicate<T> filter,T alternativeVal){
        return filter.test(src) ? alternativeVal : src;
    }


}
