package com.idea.easy.log.publisher;

import org.springframework.context.ApplicationEvent;

/**
 * @className: LogPublisher
 * @description: 事件发布者的抽象类
 * @author: salad
 * @date: 2022/6/3
 **/
public abstract class LogPublisher implements ILogPublisher{

	private ApplicationEvent event;

	public void setEvent(ApplicationEvent event) {
		this.event = event;
	}

	public ApplicationEvent getEvent() {
		return event;
	}

}