package com.idea.easy.log.utils;

import com.idea.easy.log.constant.Constant;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEvent;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @className: SpringUtil
 * @description:
 * @author: salad
 * @date: 2022/3/26
 **/
@Slf4j
@SuppressWarnings("all")
public class SpringUtil  implements ApplicationContextAware {
    private static ApplicationContext context;

    public void setApplicationContext(@Nullable ApplicationContext context) throws BeansException {
        SpringUtil.context = context;
    }

    public static <T> T getBean(Class<T> clazz) {
        try {
            return clazz == null ? null : context.getBean(clazz);
        }catch (BeansException e){
            return null;
        }
    }

    public static <T> T getBean(String beanName) {
        if(null == beanName || "".equals(beanName.trim()) || !context.containsBean(beanName)) {
            return null;
        }
        try {
            return (T) context.getBean(beanName);
        }catch (BeansException e){
            return null;
        }
    }

    public static <T> T getBean(String beanName, Class<T> clazz) {
        if (null != beanName && !"".equals(beanName.trim())) {
            return clazz == null ? null : context.getBean(beanName, clazz);
        } else {
            return null;
        }
    }

    public static ApplicationContext getContext() {
        return context;
    }

    public static void publishEvent(ApplicationEvent event) {
        if (context != null) {
            try {
                context.publishEvent(event);
            } catch (Exception e) {
                log.error(e.getMessage());
            }

        }
    }

    public static String getAppName() {
        return context.getEnvironment().getProperty("spring.application.name");
    }

    public static String getEnv() {
        if (context != null) {
            String[] activeProfiles = context.getEnvironment().getActiveProfiles();
            return activeProfiles.length > 0 ? activeProfiles[0] : null;
        }
        return null;
    }

}
